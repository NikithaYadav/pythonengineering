**Project repository for Python aspirants**



**Installations:**

**Git**

https://gitforwindows.org/


**Python**
  Python 3.7 Download and install
  https://www.python.org/ftp/python/3.7.4/python-3.7.4.exe

  Check version in cmd:
  C:\WINDOWS\system32>python --version
  Python 3.7.4

**Pycharm**
  Install Pycharm Community edition
  https://www.jetbrains.com/pycharm/download/#section=windows

  VCS -> Checkout from Version control -> Git
  Give the repository url and hit test
  Enter username and password
  Hit Clone

  Setup python interpreter:
  File -> Settings -> Project (automation_engineers) -> Add


**Bitbucket**
  Create Bitbucket account to collaborate to "PythonEngineering" repository

  *Repo url*
  https://neoswa@bitbucket.org/neoswa/pythonengineering.git


**Git push and pull steps**

  *git commit* your local work (commit to local)    [Right click the directory -> Git -> Commit]
  
  *git pull* (brings code from remote repo)         [Right click the directory -> Git -> Repository -> Pull]
  
  *git push* (pushes your changes to bitbucket)     [Right click the directory -> Git -> Repository -> Push]