import math
# 1. Dictionary looping and builtin functions:
#
# Given the below dictionary:
teams = {'Colorado': 'Rockies', 'Boston': 'Red Sox', 'Minnesota': 'Twins', 'Milwaukee': 'Brewers', 'Seattle': 'Mariners'}
#
# 1. Write a simple for loop that:
#      a. prints only the keys
#      b. prints only the values
#      c. prints the keys and values in format:
#             key1~value1
#             key2~value2
print("below are the keys of the teams dictionary")
for team in teams.keys():
    print(team)
print("below are the values of the teams dictionary")
for value in teams.values():
    print(value)
print("printing keys and values of the teams dictionary")
for key,value in teams.items():
    print(f'{key} ~ {value}')

# Given the below dictionary:
family = {'fname': 'Joe', 'lname': 'Fonebone', 'age': 51, 'spouse': 'Edna', 'children': ['Ralph', 'Betty', 'Joey'], 'pets': {'dog': 'Fido', 'cat': 'Sox'}}
#
# 2. Write a simple for loop that prints.
#    a. each child's name in a separate line
child_name=family['children']
print("family contains below children")
for name in child_name:
    print(name)
#    b. each pet's name in format:
#         "dog's name is ..."
#         "cat's name is ..."
print("family have these pets in the house")
pet_name=family['pets']
for animal,name in pet_name.items():
    print(f"{animal}'s name is :{name}")

#    c. Now add another pet of type rabbit with name bunny and pretty print the dictionary
pet_name['rabbit']='bunny'
print(family)

#    d. Now update the dict in qn. 1 to this dictionary with key as 'teams'
teams.update(family)
print(teams)

# Given the below dictionary
d = {0: 'a', 1: 'a', 2: 'a', 3: 'a'}
# 3. Using for loop print the sum of all the keys
print("sum of the all the keys")
add=0
for number in d:
    add+=number
print(add)

# 4. Use a for loop to append all the values into a single string
for value in d.values():
    final_string=''.join(value)
print(final_string)

# 5. What is the purpose of dict.clear() method? Give example.
# this method is used to remove all the content in the given dictionary.
sample_dict = {0: 'a', 1: 'a', 2: 'a', 3: 'a'}
sample_dict.clear()
print(sample_dict)
# 6. Empty all the above dictionaries and print of them. THe contents should be empty.
teams.clear()
family.clear()
d.clear()
print(f'1st dictionary:{teams}\n2nd dictionary:{family}\n3rd dictionary:{d}')

# 7. Now, delete all the above dictionaries totally from the memory.
del teams
del family
del d
# commenting below line as it giving error, remove # in the line to check the dictionaries exits in the memory.
#print(f'1st dictionary:{teams}\n 2nd dictionary:{family}\n 3rd dictionary:{d}')

# Searching items:
#
random_list =  [21.42, 'foobar', 3, 4, 'bark', False, 3.14159]
#
# Given the above list,
#
# 8.  Search and print True or False, if 5 exists in the list
if 5 in random_list:
    print("true")
else:
    print("false")

# 9.  Search and print True or False, if any number between 20 to 25 exists in the list
# @chiru: considered only integer values
for item in random_list:
    if type(item)==int:
        if item>=20 and item<=25:
            print("true")
        else:
            print("false")

# 10. Search and print if any string which has a substring of foo
for item in random_list:
    if type(item)==str:
        if 'foo' in item:
            print(f'avaliable in this string :{item}')

# 11. Search and print any boolean value
for item in random_list:
    if type(item)==bool:
        print(f'boolean value is avaliable i.e. {item}')

# 12. Search and print any number that is a perfect square. [google to find how a number is a perfect square]
for number in random_list:
    if type(number)==int:
        root = math.sqrt(number)
        if int(root + 0.5) ** 2 == number:
            print(number, "is a perfect square")
        else:
            print(number, "is not a perfect square")
# Functions
# Write a function with name "get_employee_badge"
#       1. Arguments: 3 mandatory arguments - emp_id, first_name, last_name,
#       2. Inside the function check if the emp_id parameter is an integer:
#               if not, then print "Invalid employee id: <emp_id>"
#               else, print a string that looks like: '<emp_id> : <first_name>:<last_name>'
def get_employee_badge(emp_id,first_name,last_name):
    if type(emp_id)==int:
        print(f'{emp_id} : {first_name} : {last_name}')
    else:
        print(f'Invalid employee id: {emp_id}')

get_employee_badge(51693607,'Chiranjeevi','Duvva')
# I didn't checked below solution
# Sample solution:
# def get_employee_badge(emp_id, first_name, last_name):
#     if type(emp_id) != int:
#         print('Invalid employee id:', emp_id)
#     else:
#         print(emp_id, first_name, last_name, sep=':')
#
#
# get_employee_badge('wrong', 'swadhikar', 'chandramohan')  Invalid employee id: wrong
# get_employee_badge(423367, 'swadhikar', 'chandramohan')  423367:swadhikar:chandramohan
#
#
# 13. Write a function with name "get_mail_id"
#       1. Arguments: 3 mandatory arguments - first_name, last_name, company_name
#       2. Inside the function check if the company_name parameter is a string whose size is lesser than or equals 5:
#               if the size is greater than 5, then print "Company acronym can have a max size of 5 characters"
#               else, return a mail id of format: <firstname>.<lastname>@<company>.com
def get_mail_id(first_name,last_name,company_name):
    if type(company_name)==str and len(company_name)<=5:
        print(f'{first_name}.{last_name}@{company_name}.com')
    else:
        print("Company acronym can have a max size of 5 characters")

get_mail_id('Chiranjeevi','Duvva','hcl')
get_mail_id('Raju','Merugu','cognizent')

# 14 . Write a function with name "get_emp_grade"
#       1. Arguments:
#                       2 mandatory arguments - "emp_id", "firstname"
#                       1 default argument    - "experience" with initial value of 0
#       2. Inside the function,
#               if the experience is lesser than 1, then print "<emp_id> is a fresher!"
#               if the experience is greater than 1 and lesser than 3, then return "<emp_id> is a junior developer"
#               if the experience is greater than 3, then return "<emp_id> is a senior developer"
#       3. Make three kinds of calls to this function:
#               call 1 - do not pass the parameter "experience" and print the result
#               call 2 - pass the parameter "experience" lesser than 3 and print the result
#               call 1 - the parameter "experience" greater than 3 and print the result
#
def get_emp_grade(emp_id,firstname,experience=0):
    if experience<1:
        print(f'{emp_id} is a fresher')
    elif experience>1 and experience<3:
        print(f'{emp_id} is a junior developer')
    elif experience>3:
        print(f'{emp_id} is a senior developer')

get_emp_grade(51693607,'Chiranjeevi')
get_emp_grade(51693607,'Chiranjeevi',2)
get_emp_grade(51693607,'Chiranjeevi',5)

# 15. Write a function with name "get_circle_volume"
#       1. Arguments:
#                       1 default argument - "radius" with default value 0
#       2. Inside the function,
#               if the radius is lesser than 0 or equal to 0, then return 0,
#               else return the circle volume (volume formula: (4/3) * (22/7) * r ** 3)
#       3. Make three kinds of calls to this function:
#               call 1 - do not pass the parameter "radius" and print the result
#               call 2 - pass the parameter "radius" lesser than 0 and print the result
#               call 1 - the parameter "radius" greater than 0 and print the result
#
def get_radius_volume(radius=0):
    if radius<=0:
        return 0
    else:
        return ((4/3)*(22/7)*radius**3)
result1=get_radius_volume()
print(f'radius {result1}')
result2=get_radius_volume(-1)
print(f'radius {result2}')
result3=get_radius_volume(2)
print(f'radius {result3}')

# 16. Write a function with any name that accepts one default argument at first
#     followed by a mandatory argument and note the result. (hint: f(d=10, m) )
# def area(length=5,width):
#     print(f'area is :{2*length*width}')
# area(2,3)
# SyntaxError: non-default argument follows default argument

# 17. Write a function with any name that accepts one default argument at first
#     followed by a mandatory argument and note the result. (hint: f(d=10, m) )
#  both 16 and 17 questions lookin same

# Sample question for *args:
# Write a function that accepts any number of integer arguments and returns the product of all its numbers
# def get_product(*numbers):
#     product = 1
#     for number in numbers:
#         product = product * number
#     return product
#
# 18. Write a function that accepts any number of integer arguments and returns the difference of all its numbers
def get_difference(*numbers):
    difference=0
    for number in numbers:
        difference=difference-number
    return difference
result=get_difference(2,3,4,5,6)
print(f'difference is :{result}')

# 19. Write a function that:
#       a. Accepts one mandatory argument - student_id
#       b. Any number of marks as *marks
#    Inside the function check:
#       c. If *marks has any content, then calculate the sum of all marks and return total marks of a student
#       d. Else, return 0
def student_marks(student_id,*marks):
    if len(marks)>0:
        total=0
        for mark in marks:
            total=total+mark
        return total
    else:
        return 0

total_marks=student_marks('chiranjeevi')
print(f'total marks are :{total_marks}')
total_marks=student_marks('nikitha',10,20,30,40)
print(f'total marks are :{total_marks}')


# 20. Write a function that has any number of keyword arguments (kwargs) that has keys
#    as student ids in a classroom and his/her marks as value. For example, 'Student'=1800, 'Student2'=900
#    The function should return the total marks scored by all students in the class
def students_marks_class(**details):
    total=0
    for marks in details.values():
        total=total+marks
    return total
total_marks=students_marks_class(chiru=100,niki=200,govind=300)
print(f'toatal marks of class :{total_marks}')

# 21. Write a function that has:
#       Arguments:
#           a. party name (mandatory)
#           b. variable number of keyword arguments with district names as keys and votes secured as values
#                   For example: chennai=18000, trichy=7800 and so on.
#       The function should calculate the total votes and print "<partyname> has got a total of <totalvotes> votes'
def total_votes(party_name,**district_votes):
    total_votes=0
    for votes in district_votes.values():
        total_votes=total_votes+votes
    print(f'{party_name} has got a total of {total_votes} votes')

total_votes('TRS',wgl=1000,hyd=1200,kmm=700,kmgr=900)


# 22. Write a function that has:
#       Arguments:
#           a. party name (mandatory)
#           b. variable number of keyword arguments with district names as keys and votes secured as values
#               For example: chennai=18000, trichy=7800 and so on.
# #
#       The function should:
#           1. Check if the **kwargs has any content in it. If not, then simply return the function
#           2. else, calculate the total votes and print "<partyname> has got a total of <totalvotes> votes'
def total_votes2(party_name,**district_votes):
    if len(district_votes)>0:
        total_votes = 0
        for votes in district_votes.values():
            total_votes = total_votes + votes
        print(f'{party_name} has got a total of {total_votes} votes')
    else:
        return None

print(f"total votes of the party is :{total_votes2('TRS')}")