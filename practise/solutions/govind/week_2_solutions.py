# @swadhi: Generic comments which I have done this time
#
# 1. CTRL + ALT + L will auto format the code and make it look tidy!
# 2. Use '#' and """ """" to comment no python code, for example,
#   o/p: ...
"""
o/p:
    ...
    ...
"""

# 1. Create a dictionary variable "mustang", with following values and print it
# "brand"="Ford",
# "model"="Mustang",
# "year"=1964

mustang = {"brand": "Ford", "model": "Mustang", 'year': 1964}
print(mustang)

# Using the above dictionary, print the below line
# "Mustang" is the model that "Ford" had released in the year - 1964

mustang = {"brand": "Ford", "model": "Mustang", "year": 1964}
print(mustang["model"], 'is the model that had released in the year - ', mustang["year"])
# @swadhi: consider using the "f" string method of printing


# 2. Create a dictionary student_ages with below values and print the size of the dictionary
# 'Tim'= 18,'Charlie'=12,'Tiffany'=22,'Robert'=25

student_ages = {'Tim': 18, 'Charlie': 12, 'Tiffany': 22, 'Robert': 25}
print(len(student_ages))

# 3. Add two more students, "Sarah"=9 and "John"=21 to the above dictionary
student_ages = {'Tim': 18, 'Charlie': 12, 'Tiffany': 22, 'Robert': 25}
student_ages['Sarah'] = 9  # @swadhi: for multiple item addition, always use dict.update method
student_ages['John'] = 21
print(student_ages)

# 4. From the above dictionary, modify the student 'Robert's age using:
#         i. Normal approach
#         ii. Update method

student_ages = {'Tim': 18, 'Charlie': 12, 'Tiffany': 22, 'Robert': 25}
student_ages['Robert'] = 45
print(student_ages)

student_ages = {'Tim': 18, 'Charlie': 12, 'Tiffany': 22, 'Robert': 25}
student_ages_update = {'Robert': 80}
student_ages.update(student_ages_update)
print(student_ages)

# 5. To the student_ages dictionary, add any new key which is of type list and note the error

student_ages = {'Tim': 18, 'Charlie': 12, 'Tiffany': 22, 'Robert': 25, ['a', 'b', 4, 1.34]: 5}
print(student_ages)  # key should be immutable

# TypeError: unhashable type: 'list'    # @swadhi: perfect! this is what I was expecting. Elegantly done.


# 6. Create an empty dictionary ip_address and add the below values to it one by one.
# 'lab'= '192.1.1.1'
# 'ecs'= '192.1.1.2'
# 'bpo'= '192.1.1.3'
# 'rmg'= '192.1.1.4'
ip_address = {}
ip_address['lab'] = '192.1.1.1'
ip_address['ecs'] = '192.1.1.2'
ip_address['bpo'] = '19.1.1.3'
ip_address['rmg'] = '19.1.1.4'
print(ip_address)

# o/p : {'lab': '192.1.1.1', 'ecs': '192.1.1.2', 'bpo': '19.1.1.3', 'rmg': '19.1.1.4'}


# 7. To the above dictionary, add three more ip address at one shot.
# 'ise': '192.1.1.5'
# 'wsa': '192.1.1.6'
# 'wsg': '192.1.1.7'
ip_address = {}
ip_address['lab'] = '192.1.1.1'
ip_address['ecs'] = '192.1.1.2'
ip_address['bpo'] = '19.1.1.3'
ip_address['rmg'] = '19.1.1.4'

ip_address_one_shot = {'ise': '19.1.1.5', 'wsa': '19.1.1.6', 'wsg': '192.1.1.7'}
ip_address.update(ip_address_one_shot)
print(ip_address)

# o/p:{'lab': '192.1.1.1', 'ecs': '192.1.1.2', 'bpo': '19.1.1.3', 'rmg': '19.1.1.4',
# 'ise': '19.1.1.5', 'wsa': '19.1.1.6', 'wsg': '192.1.1.7'}

# 8. Write a program that
#   a. uses a data type to store marks of 5 students with name as key and their mark as value
#   b. loop through the dictionary and print the marks alone

student_marks = {'Govind': 10, 'Swadhikar': 20, 'Brindha': 30, 'Chiru': 40, 'Nikitha': 50}
print(student_marks)

for m in student_marks.values():    # @swadhi: do not use character variables like 'm'. 'mark' or 'student_mark'
    print(m)                        # The general rule is that variable name should imply the type of value it holds

# o/p:
# {'Govind': 10, 'Swadhikar': 20, 'Brindha': 30, 'Chiru': 40, 'Nikitha': 50}
# 10
# 20
# 30
# 40
# 50


# 9. Write a program that
#   a. Uses a data type to store firstname, lastname and monthy income as key and their corresponding value as values
#   b. Add another key named 'countries_visited' and set empty list as value
#   c. Add three countries to 'countries_visited'

employee_list = {'firstname': 'Govind', 'lastname': 'Kamalakaluva', 'monthlyincome': 500000}

employee_list['countries_visited'] = []

employee_list['countries_visited'] = [' Australia', 'Thailand , America']

print(employee_list)

# @swadhi: consider using pprint.pprint() going forward!
# o/p:{'firstname': 'Govind', 'lastname': 'Kamalakaluva', 'monthlyincome': 500000,
# 'countries_visited': [' Australia', 'Thailand , America']}

# 10. Write a program that
#   a. Uses a data type to store marks of three students like below and store it in a variable.
#   'student1'=<mark1>,<mark2>
#   'student2'=<mark1>,<mark2>
#   'student3'=<mark1>,<mark2>
#
#   b. For each student add another mark (of your choice to their marks list)

marks_students = {'student1': [50, 60], 'student2': [80, 90], 'student3': [70, 60]}
print(marks_students)
for m in marks_students:
    marks_students[m].append(100)
    print(marks_students)

# o/p:{'student1': [50, 60], 'student2': [80, 90], 'student3': [70, 60]}
# {'student1': [50, 60, 100], 'student2': [80, 90], 'student3': [70, 60]}
# {'student1': [50, 60, 100], 'student2': [80, 90, 100], 'student3': [70, 60]}
# {'student1': [50, 60, 100], 'student2': [80, 90, 100], 'student3': [70, 60, 100]}


# 11. from the dictionary created in previous question
#   a. loop through student1's mark and print the total
#   b. loop through student2's mark and print the total
#   c. loop through student3's mark and print the total

marks_students = {'student1': [50, 60], 'student2': [80, 90], 'student3': [70, 60]}

for n in marks_students.values():   # @swadhi: as like above, variable naming 'n', 'i' and 'j'
    i = n[0]                        # should be self-explanatory
    j = n[1]
    x = i + j

    print(x)

# o/p:
# 110
# 170
# 130

# 12. from the above dictionary print the mark of a student who does not exists and note the error thrown

marks_students = {'student1': [50, 60], 'student2': [80, 90], 'student3': [70, 60]}

print(marks_students['student4'])

# o/p:KeyError: 'student4'


# 13. from the above dictionary print the mark of a student who does not exists:
#   if the student does not exists then print None
#   if the student does not exists then print 'Student Does Not Exists: <student name>'

marks_students = {'student1': [50, 60], 'student2': [80, 90], 'student3': [70, 60]}

print(marks_students.get('student4', 'None'))

# o/p:None

marks_students = {'student1': [50, 60], 'student2': [80, 90], 'student3': [70, 60]}

print(marks_students.get('student4', 'Student Does not Exists:student4'))

# o/p:Student Does not Exists:student4


# 14. from the above dictionary, print the following:
#     print only the keys
#     print only the values
#     print all the key value pairs as a list of tuples

marks_students = {'student1': [50, 60], 'student2': [80, 90], 'student3': [70, 60]}

print(marks_students.keys())
print(marks_students.values())
print(marks_students.items())

# 15. Traverse the above dictionary and print all the student records in below format.
#     student1 has secured the marks [mark1, mark2, mark3]
#     student2 has secured the marks [mark1, mark2, mark3]
#     ...

marks_students = {'student1': [50, 60], 'student2': [80, 90], 'student3': [70, 60]}
for v in marks_students:
    print(f'student1 has secured the marks', marks_students[v])

# 16. Write a program that
#   a. Uses a data type to store languages known by five programmers and store it in a variable
#   (Hint: name is key, value is iterable)
#   b. Loop through the data and print programmer names who knows python (if inside a for loop)

programmers = {'Govind': ['C', 'Python'], 'Brinda': 'Java', 'Nikita': 'c++', 'Chiru': 'Python', 'Swadkar': '.net'}

for k, v in programmers.items():
    if 'Python' in v:
        print(k)

# o/p:       Govind
# Chiru


# 17. Using the above dictionary, print the programmer names who knows more than 2 programming languages
programmers = {'Govind': ['C', 'Python'], 'Brinda': ['Java', 'Python'], 'Nikita': ['c++', 'java', 'Python'],
               'Chiru': 'Python', 'Swadkar': '.net'}

# 18. Use the above dictionary to:
#   a. access an in-existent programmer
#   b. if a programmer don't exists, then print 'ProgrammerNotFoundError'

programmers = {'Govind': ['C', 'Python'], 'Brinda': ['Java', 'Python'], 'Nikita': ['c++', 'java', 'Python'],
               'Chiru': 'Python', 'Swadkar': '.net'}

print(programmers.get('Govind'))

print(programmers.get('Kaluva', 'ProgrammerNotFoundError'))

# 19. In the above dictionary, use a single command to return the languages known by any programmer and also to remove him from the data

programmers = {'Govind': ['C', 'Python'], 'Brinda': ['Java', 'Python'], 'Nikita': ['c++', 'java', 'Python'],
               'Chiru': 'Python', 'Swadkar': '.net'}

print(programmers['Govind'])

del programmers['Govind']
print(programmers)

# @swadhi: Could break into multiline dict definition like below for readability
# programmers = {
#   key1: value1,
#   key2: value2
# }
programmers = {'Govind': ['C', 'Python'], 'Brinda': ['Java', 'Python'], 'Nikita': ['c++', 'java', 'Python'],
               'Chiru': 'Python', 'Swadkar': '.net'}
print(f'Govind knows the following languages :', programmers['Govind'])

# 21. You want to store a collection of repetitive names to be accessed later on.
# What data type  you will use?
#  tuple                # @swadhi: list or a tuple

dup_tup = ('Govind', 'Govind', 1, 1)
print(dup_tup)

# 22. You want to store a collection of unique phone numbers. Which data type  will you use?
# Give example
# set
phone_no = {'9790902567', '9790902567'}
print(phone_no)

# 23. You are given with a set of constant values such as scores of students in a classroom
# this scores might be used in the future for forecasting purposes. Which data type will you use?

# tuple

constant_values = (50, 60, 70, 80)
print(constant_values)

# 24. You are provided with roll numbers and total scores of students in class 12th.
# In the future, the data might be accessed based on roll numbers. Which data type you will use?

# Dictionary

roll_numbers = {'1507': 100, '1508': 70}
print(roll_numbers)

# 25. You need to store the runs scored by kohli in a data type such that after every match
# you have to enter a value. Which data type will you consider?

# list

kohli_runs = [150, 200, 70, 96, 120]
print(kohli_runs)                   # @swadhi: perfect!