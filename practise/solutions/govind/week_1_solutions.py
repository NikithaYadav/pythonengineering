"""
    @swadhi: Please copy and paste the questions and solve them.
    Leave the questions be commented and answers uncommented.
    Refer: https://bitbucket.org/neoswa/selenium_training/src/master/session_1/anand_ans.py
    It will be easier for review and to refer later.
"""
#1. Is python dynamically typed Language or statically typed?
Python is dynamically typed  langauage .

#2. What is dynamic typing means? Is it efficient than statically typed language?
Dynamically typed  means we no need to mention data type for an variable . Python interpets
the datatype accordingly . It is very useful as development perspective but as an interpreter perpective it is not
efficient as interpreter need to do extra step to interpret the type .

#3. What is the difference between integer and float variable?
Integer is any postive number . Float is an any  number that is not positive  ex : 3.14  , -5  etc.,

#4. Which of these are core data types in python?
    # (A) Lists
    # (B) Dictionary
    # (C) Tuples
    # (D) Class
List , Dictionery , Tuple are core data types

# 5. What data type is the object below ?
#     L = [1, 23, ‘hello’, 1]
#     (A) List
#     (B) Dictionary
#     (C) Tuple
#     (D) Array

It is List and it contains non homogeneous elements which means list may contain mutiple values with different data
types

# 6. Which of the following function display's datatype of variables in python?
#     (A) datatype(variable)
#     (B) type(variable)
#     (C) str(variable)
#     (D) id(variable)
type(variable)

# 7. Create a string variable 'text' and print it. The
#    output should be: Hello World

first_message = ' Hello World '
print(first_message)

# 8. Create a string variable 'special_string' and print it. The
#    output should be:
#
#    Python is an "object-oriented" Programming language

special_string =''' Python is an "object-oriented" Programming language'''
print(special_string)

# 9. Create a string variable 'multiline_string' and print it. The
#    output should be a multiline string with one line spacing.
#
#    Hint: You may use triple quoted string instead of \n new line character
#
#    line 1: Unlike other high level languages
#    line 2: <blank line>
#    line 3: Python is interpreted

multiline_string = '''Unlike other high level languages
Python is interpreted'''
print(multiline_string)

# 10. Create a string variable "news" with value below:
#     Air travel to become bit costlier as govt announces hike in aviation security fee from July 1
#
#     Question: Using string slicing print the below values:
#               Air travel
#               July 1
#
#     Sample:
#       print(news[:n])    empty index at left of : denotes start
#       print(news[-n:])   empty index at right of : denotes end

news='Air travel to become bit costlier as govt announces hike in aviation security fee from July 1'
print(news[:10])
print(news[-6:])

# 11. Given two string below:
#
#     job_title = "job requirement"
#     certs = "certifications in administration is desired"
#     added_certs = "additional certifications as below is a plus but not required"
#     skills_1 = "azure, windows server"
#     skills_2 = "aws, dotnet"
#
#     Use string methods to
#       1. capitalize start of each word in job_title
#       2. capitalize the first letter of the string certs
#       3. capitalize the string completely added_certs
#       4. Make the start of each word in skills_1 capitalized
#       5. Make the all the words capitalized skills_2

 job_title = "job requirement"
print(job_title.title())
certs = "certifications in administration is desired"
print(certs.capitalize())
 added_certs = "additional certifications as below is a plus but not required"
 print(added_certs.upper())
 skills_1 = "azure, windows server"
print(skills_1.title())
skills_2 = "aws, dotnet"
print(skills_2.upper())

# Questions - 2: Datatype definitions

# # 2. Define a string variable "name" with your name in all lower case as value and print it.
#     Now, use a string method to convert it into all upper case and print it
name = 'govind kamalakaluva'
 print(name.upper())           # @swadhi: assigning it to a variable and then printing makes the variable reusable

# 3. Define a string variable "name" with your fullname (firstname<SPACE>lastname) in all lower case as value
#     and print it. Now, use a string method to convert each of the first word into capital case and print it.
#     Hint: str.title() function
name = 'govind kamalakaluva'
 print(name.title())            # @swadhi: assigning it to a variable and then printing makes the variable reusable

# 4. Given the below list, use a string function to convert it into a single string separated by single space.
#     usernames = ['swadchan', 'vivevija', 'thasomas', 'verajend']
#     Output:     'swadchan vivevija thasomas verajend'

usernames = ['swadchan', 'vivevija', 'thasomas', 'verajend']
 print(' '.join(usernames))

#  5. Given the below list, use a string function to convert it into a single string separated by comma.
#     passwords = ['guessme', 'p@$$W0rd', 'hackme']
#     Output:     'guessmep@$$W0rdhackme'
passwords = ['guessme', 'p@$$W0rd', 'hackme']
print(','.join(passwords))

#  6. Define a string variable "text" with value "thIs iS a saMPle tEXt" and print it.
#     Use a string function to convert it to lowercase and reassign it to 'text'. (text = text.<function>() )
#     Now replace the word "a sample" with "an example" and print it.
text="thIs iS a saMPle tEXt"
 print(text)
 new_text = text.lower()
 print(new_text)
 print(new_text.replace('a sample','an example'))

#  7. Define a list with any 5 numbers. Add any number to the end of this list. (Hint: list.append() )
add_number=[1,2,3,4,5]
 print(type(add_number))
 add_number.append(6)
print(add_number)

#  8. Define a list with any 5 words (strings). Add any number to the end of this list.
add_word=['govind','swadikar','chiru','nikita','brinda']
 print(add_word)
 add_word.append('nirmal') # @swadhi: the question is to add a number at the end :)
 print(add_word)

#  9. Define a list with 3 floating point numbers. Add two more floating point values to the end of this list.
#     (Hint: list.extend())
add_float=[1.1 ,1.2 ,1.3]
print (type(add_float))
add_float.extend([1.4,1.5])
print(add_float)

#  10. Given a below list with duplicates.
#      browsers = ['chrome', 'firefox', 'safari', 'chrome', 'ie', 'ie']
#            a. Print the number of times 'chrome.exe' has appeared in the list.
#            b. Sort the list and print it
browsers = ['chrome', 'firefox', 'safari', 'chrome', 'ie', 'ie']
 print(browsers.count('chrome'))
 browsers.sort(reverse=False)  # @swadhi: reverse=False by default so it is not required here
 print(browsers)

#  11. Given a below list.
#      processes = ['chrome.exe', 'mcafee.dll', 'notepad.exe']
#            a. Add a new process 'cmd.exe' to the start of the list and print it
#            b. Print the number of processes

 processes = ['chrome.exe', 'mcafee.dll', 'notepad.exe']
 processes.insert(0,'cmd.exe')
 print(processes)
 print(len(processes))

#  11. Given a below list.
#      processes = ['chrome.exe', 'mcafee.dll', 'notepad.exe']
#            a. Add a new process 'cmd.exe' to the start of the list (at index 0) and print it  (hint: list.insert)
#            b. Add 2 more new processes 'eclipse.exe' and 'norton.exe' to the end and print it (hint: list.extend)
#            b. Print the number of processes in the list
 processes = ['chrome.exe', 'mcafee.dll', 'notepad.exe']
 processes.insert(0,'cmd.exe')
 print(processes)
 new_processes = ['ecipse.exe','norton.exe']
 processes.extend(new_processes)
 print(processes)
 print(len(processes))

#  12. Create a tuple with 3 values: 'anand', 30, 'cts'
 new_tup=('anand',30,'cts')
 print(type(new_tup))

#  13. Create a tuple with 1 value: 'anand'
 sing_tuple=('anand',)
 print(type(sing_tuple))
 print(sing_tuple)

#  14. Create an empty tuple with 0 values hint: ()
 empty_tuple=()
print(type(empty_tuple))

#  15. Create a tuple variable "colors" with below values and print number of times red has occurred.
#      (hint: len() will not work)
#        red
#        green
#        green
#        orange
#        yellow
#        yellow
#        white
#        red

colors=('red','green','green','orange','yelow','yellow','white','red')
 print(colors)
 count_red=colors.count('red')
 print(count_red)

#  16. Try to modify the first element of this tuple using below command and see what error occurs.
#      colors[0] = 'pink'
colors=('red','green','green','orange','yelow','yellow','white','red')
 print(colors)
 modify_first=colors[0]='pink'
 print(modify_first)
 TypeError: 'tuple' object does not support item assignment

#  17. Try to add "colors" by itself as below and print the value.
#      (hint: this will create a new tuple. since tuple is immutable)
#      colors = colors + colors

 colors=('red','green','green','orange','yelow','yellow','white','red')
 print(colors+colors)
('red', 'green', 'green', 'orange', 'yelow', 'yellow', 'white', 'red', 'red', 'green', 'green', 'orange', 'yelow', 'yellow', 'white', 'red')

#  18. From the given set of political parties, create a set with variable name "parties" and print it.
#      congress
#      congress
#      bjp dmk
#      admk
#      dmk
#      congress
#      admk
#      congress
#      dmk
parties = {'congress','congress','bjp dmk','admk','dmk','congress','admk','congress','dmk'}
 print(parties)

#  19. Given the below list of designations of team members. Find all the unique designations from this list.
#      designations = ['developer', 'senior developer', 'developer', 'manager', 'senior developer', 'tester']

 designations = ['developer', 'senior developer', 'developer', 'manager', 'senior developer', 'tester']
 designations=set(designations)
 print(list(designations))

#  20. Does set allow indexing?
 no    # @swadhi: it is better to specify the reason. Since, set and dict ar hashable, indexing is not possible

#  21. Does list or tuple allow indexing?
 yes

#  22. Given the below "input_set" do the following:
#      input_set = {'python', 'java', 'perl', 'java', 'python', 'java'}
#          a. add a new language "dotnet" to the above set
#          b. add the below list of languages to the set
#              ['php', 'oracle', 'php', 'swift']

 input_set = {'python', 'java', 'perl', 'java', 'python', 'java'}

 input_set= input_set | {'dotnet'}

 print(input_set)

 new_set = input_set |  set(['php', 'oracle', 'php', 'swift'])

 print(new_set)

#  23. Given the below "input_set" do the following:
#      input_set = {'python', 'java', 'perl', 'java', 'python', 'java'}
#          a. remove the language "dotnet". If not found an error should be thrown
#          b. remove the language "dotnet". If not found an error should NOT be thrown
 input_set = {'python', 'java', 'perl', 'java', 'python', 'java'}
 remove_set=input_set.remove('dotnet')
 print(remove_set)

 input_set = {'python', 'java', 'perl', 'java', 'python', 'java'}
 remove_set=input_set.discard('dotnet')
 print(remove_set)

#  24. Create a list of numbers from 0 to 9 using range(n) function and print. (do list(range))
 print_range = list ( range(0,10))
 print(print_range)

#  25. Create a list of numbers from 10 to 19 using range(n) function

# print_range = list(range(10,20))
 print(print_range)

#  26. Create a list of numbers with variable name "list_of_tens" from 10 to 100
#      (including 100) in steps of 10 using range(n) function

 list_of_tens=list(range(10,110,10))
 print(list_of_tens)
 [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

#  27. From the list created in question 26, print the size of the list

 size_list=[10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
size_list=len(size_list)
 print(size_list)

#  28. Write a for loop to print the elements in list "list_of_tens" line by lines
 list_of_tens=[10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
 for i in list_of_tens:
     print(i)

#  29. Enhance this for loop to print only the numbers divisible by 4 and 8.

 list_of_tens=[10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

 for i in list_of_tens:
     if i%4 and i%8 :      # @swadhi: we haven't seen if else yet. we can discuss this in upcoming sessions
         print(i)

#  30. Create a list of odd numbers from 40 to 99.
#      Traverse over this loop and print the first number that is divisible by 7

odd_number = list(range(40, 100))
for i in odd_number:
    if  i%7 ==0 and i % 2 != 0 :
        print(i)
