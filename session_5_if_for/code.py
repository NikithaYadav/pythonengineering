# """
#     - Boolean keywords
#         - and, or, not
#
#     Comparison operators
#
#       Operator          Example
#          >	             x > y
#          <	             x < y
#          ==	             x == y
#          !=	             x != y
#          >=	             x >= y
#          <=	             x <= y
#          is              x is y
#          is not          x is not y
#
#     - Conditional statements and loops
#         - if [, else]
#         - if - elif - else (nested if)
#         - for loop
#         - while loop
#         - pass, break and continue
#
#     - is keyword
# """
is_connected = True
is_hosts_up = False

and_res = is_connected and is_hosts_up
or_res = is_connected or is_hosts_up

print(not True)  # True
print(not False)  # False

# >, <
# ==, !=
# >=, <=
# is, is not
# - if [, else]


# - if - elif - else (nested if)
# if (condition) {
#
# }

# 0 is equivalent to false
# all negative and positive numbers are True

condition = True
# l = []
l = [1]

# Only one condition [if]
# Only two conditions [if-else]
if len(l) == 0:  # boolean condition
    # Condition is met
    print('empty list')
else:
    # Condition is not met
    print('List has some contents')

# https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjx0Oy5luHlAhWKLI8KHQM6AcIQjRx6BAgBEAQ&url=https%3A%2F%2Fcommons.wikimedia.org%2Fwiki%2FFile%3AIf-Then-Else-diagram.svg&psig=AOvVaw0GDws-7qf-WAtmpDl26pZE&ust=1573527898724311
# print('Remaining code')

# Multiple conditions [if-elif-else] (nested if else)
if 5 < 3:  # cond 1
    print('Not printed 1')
elif 4 > 3:  # cond 2
    print('Not Printed 2')
elif 10 % 3 == 0:  # cond 3
    print('Not printed 3')
else:
    print('No conditions were met')

# Multiple condition checks must be promoted to the top
is_hosts_up = True

if is_connected and is_hosts_up:  # similarly or
    print('We are connected to the network and all other hosts were connected')
    # connect_to_network()
    # processing()
elif is_connected:
    print('We are connected to the network')
    # wait()
else:
    # default option
    print('No connections were made')

if 5 / 2 == 2.5:
    print('True')
elif 6 % 2 == 0:
    print('Elif')
else:  # Defaults to all other conditions
    print('else')

# - for loop
# Loop a list example
# Loop a dict example

l = [1, 2, 3]

# print(n)    # NameError

# a, *_, b = 5, 6, 7, 8, 9
# print(a)
# print(b)
# print(_)

# for n in l:
for n in range(1, 21):
    # for m in range(): # n * n complexity
    squared = n ** 2
    print(squared)

    if squared % 2 != 0:  # even nos. are divisible by 2
        print(f'{squared} is an odd square!')
    else:
        print(f'{squared} is an even square!')

for x in "banana":
    print(x, end=':')

for n in set(l):
    print(n)

for d in {'a': 1, 'b': 2, 'c': 3}:  # looping a dict will loop through it's keys
    print(d)

# import time
# for _ in range(10): # 0 - 9
#     time.sleep(0.25)
# print(_)    # 9


# for (int idx = 0; idx < len(l); idx++) {
#     print(l[idx])
# }


# - while loop
# range() and number divisible by 11 example

# - pass, break and continue

# break - exits a for loop
for i in range(10):
    print(i)
    if i == 5:  # filter to break the for
        break
# for


# continue - invokes loop once again
for i in range(1, 11, 2):
    if i % 2 == 0:  # if a no. is even, just ignore it and do not run remaining code
        continue  # to re-invoke the loop, without executing the rest of the code
    print(i, end=' ')

print()

    for i in ['rajesh', 'hari', 'govind']:
        if len(i) < 5:
            continue
        print(i, end=' ')

# odd or even nos. using range function
# >>> list(range(2, 11, 2))
# [2, 4, 6, 8, 10]
# >>> list(range(1, 11, 2))
# [1, 3, 5, 7, 9]
