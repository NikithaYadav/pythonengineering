"""
    - Tuple
    - Set and common set methods:
        - add(item)
        - update(list)
        - remove(item)  - removes the only occurrence, if not found: KeyError
        - discard(item) - remove if item exists. else ignores

    - len() function
    - del keyword
"""

# ---- Tuple ---- (Read only list)
# Immutable
# Similar to lists in all other aspects

t = (1, 2, 3)

print('tuple definition:', t)
print('tuple type:', type(t))    # # class <tuple>

# When to use?
# The elements will not be added or modified
students = ('gov', 'nik', 'chir', 'brin')

# Single element tuple
# n = (5 * 10) + 5
n = (50)
print(n)        
print(type(n))    # class <int>

single_tuple = (50,)
print(type(single_tuple))    # class <tuple>

tuple_of_3_nos = 50, 100, 150
print(type(tuple_of_3_nos))  # class <tuple>

# Tuple unpacking
# int a = 50
# int b = 100
# int c = 150
a, b, c, *_, last = 50, 100, 150, 200, 300, 400, 500
print(a)       # 50
print(b)       # 100
print(c)       # 150
print(last)    # 500

# Empty tuple
empty_tuple = ()
print(type(empty_tuple))    # class <tuple>

# Tuple from a list
students = [
    'chiru',
    'chiru',
    'swadhi',
    'govind'
]

students_tuple = tuple(students)  # Type cast into tuple
print(students_tuple)
print(type(students_tuple))

# ** Difference between list and tuple
# Lists are mutable and Tuples are immutable
# <Look at the end of the file for detailed differences>

# for attribute in (dir(tuple)):
#     if not attribute.startswith('__'):
#         print(attribute)
#
# print('\nLists:')
# for attribute in (dir(list)):
#     if not attribute.startswith('__'):
#         print(attribute)
# """
# Lists:
# append
# clear
# copy
# count
# extend
# index
# insert
# pop
# remove
# reverse
# sort
# """


# index function and count
print(students_tuple.count('govind'))    # fetches the number of occurrences of 'govind'
print(students_tuple.count('swadhi'))
print(students_tuple.index('swadhi'))
print(students_tuple.index('chiru'))

# ----  Sets  ----
# Mutable
# Sets are unordered
# Duplicate elements are not allowed
# Set allows only immutable types

random_set = {42, 'foo', 3.14159, None}
authors = {'Smith', 'McArthur', 'Wilson', 'Johansson'}
print(random_set)
print(authors)


# How to define an empty set?
empty_set = {}
print(type(empty_set))  # dict - { k: v, k: v, .. }

empty_set = set()
print(type(empty_set))

# Set from a list
# Write a program that takes in a list and remove all duplicate entries
duplicate_list = ['tcs', 'cts', 'hcl', 'tcs', 'tcs', 'hcl', 'syntel']
print(list(set(duplicate_list)))

# print({'a', 'a', 'a', 'a'})


# Write a program that checks a list and finds the number of hundreds
scores = [100, 98, 76, 40, 100, 87, 50]
# print('No. of people scoring hundred:', scores.count(100))

# Unique scores
print(list(set(scores)))
# Make sure that set has only one occurrence

# Set allows only immutable types
l = [1, 2, 'a', 'b']
s = {l}   # not like set(l) # TypeError: unhashable type: 'list'


# - Set and common set methods:
#     - add(item)
#     - update(list)
#     - remove(item)  - removes the only occurrence, if not found: KeyError
#     - discard(item) - remove if item exists. else ignores
#     - union(set)
#     - intersection(set)
scores_set = set(scores)

print(scores_set)
scores_set.add(80)
print(scores_set)

# Update a list of scores
new_scores = [75, 51, 63, 51, 63, 84]
scores_set.update(new_scores)
print(scores_set)

# Remove a particular item
scores_set.remove(100)
print(scores_set)

# Difference between a remove and discard method in set
# scores_set.remove(100)      # KeyError: 100
scores_set.discard(100)  # If present then remove, else discard

# print({1, 2, 3}.union({3, 4, 5}))
print({1, 2, 3} | {3, 4, 5})

# print({1, 2, 3}.intersection({3, 4, 5}))
print({1, 2, 3} & {3, 4, 5})

# Tuple vs List
# Major Difference:
# Tuples are fixed size in nature whereas lists are dynamic.
# In other words, a tuple is immutable whereas a list is mutable.

# Other differences
# You can't add elements to a tuple. Tuples have no append or extend method.
# You can't remove elements from a tuple. Tuples have no remove or pop method.
# You can find elements in a tuple, since this doesn’t change the tuple.
# You can also use the in operator to check if an element exists in the tuple.
# tuples can be used as dictionary keys
# Tuples are faster than lists
# It makes your code safer if you “write-protect” data that does not need to be changed.
