"""
    Dynamic typing

    data types:
        bool, str, int, float, list, tuple, set, dict

    object orientation in python

    Mutability: Mutable vs Immutable
"""

"""
Dynamically typed:

Dynamic --> at runtime -> data type is detected

Java:
    int a = 100
    float pi = 3.14
    String name = "Govind"

Python:
    a = 100     (interpreter)
    pi = 3.14
    name = 'swad'
"""
# detect the data type
# assign memory to it (store it in memory)

# # Perl, kotlin, go, Ruby and so on
a = 100
print(a)
print(type(a))

pi = 3.14
print(pi)
print(type(pi))

"""
    Defining Data types
        bool
        int
        float
        str
"""
# <var> = <some_data>
a = 5  # mem alloc, data type detection
print(id(a))

n = 'welcome'
print(id(n))

# bool
is_light_on = True  # isLightOn (Java)
print(is_light_on)
print(type(is_light_on))  # <class 'bool'>

is_light_on = not is_light_on
print(is_light_on)
print(type(is_light_on))  # <class 'bool'>

# numbers - int and float
pi = 22 / 7
print(pi)
print(type(pi))

# How you will do integer division in python
pi_rounded_off = 22 // 7  # Integer division - py2:     int(22/7) - type casting
# pi_rounded_off = int(22 / 7)  # Integer division - py2:     int(22/7) - type casting
# pi_rounded_off = 3
print(pi_rounded_off)
print(type(pi_rounded_off))

# str - collection of english characters within single quotes or double quotes or even triple quotes

welcome_msg = 'Welcome to Python Programming'
# welcome_msg = "Welcome to Python Programming"
print(welcome_msg)
print(type(welcome_msg))

welcome_msg = "Welcome to Python's World"
# welcome_msg = "Welcome to Python Programming"
print(welcome_msg)
print(type(welcome_msg))

error_msg = "Weekly job's runtime is: 8 min"
print(error_msg)
print(type(error_msg))

# Multi-line string
ml_string = 'This is the first line of doc\n' \
            'and this is the second'

# triple quotes - three single quotes - three double quotes
ml_string = '''This is the first line of doc
and this is the second
and this is the 3rd'''
print(ml_string)

"""
        Mutable:
            - Mutable data types can grown, reduced or modified within itself
            - Example:
                - list
                - dict
                - set 
                
        Immutable:
            - Immutable objects cannot be grown or modified
            - It required a new object being created whenever it is modified
            - Example:
                - int 
                - float
                - bool 
                - str 
                - tuple 
"""
# # Immutable
# a = 5
# print(id(a))    # the integer cannot change its value within itself - it requires an other object in memory
# a = 6
#print(id(a))
#
# a = 10
# b = 10
# print(id(a))
# print(id(b))

# strings
print('string example:')
name = 'swad'
print(id(name))

name = 'swadhikar'
print(id(name))

# Mutable
# list
random_list = ['Swadhi', 50, [1, 2, 3, 4], '<SPACE>', True]
print(id(random_list))

random_list.append(None)

print(id(random_list))

# set
random_set = {1, 'name'}
print(id(random_set))

random_set.add('age')
print(id(random_set))

# dict
car_prices = {'BMW': 1000, 'Ferrari': 2000, 'Toyota': 500}
print(id(car_prices))
# print(car_prices['BMR'])
car_prices['Lamborghini'] = 5000
print(id(car_prices))

"""
    Defining data types
        list
        tuple
        dict
        set
"""
# list - sequence of elements of any data type which grow or shrink (mutable) - square brackets
# str  - sequence of characters
# 0         1   2              3   4
random_list = ['Swadhi', 50, [1, 2, 3, 4], '<SPACE>', True]
#               -5      -4    -3            -2        -1

print(random_list)
print(type(random_list))

print(len(random_list))

# Access elements
print(random_list[2])

# last element in the list

# size = list.size - 1
print(random_list[-1])
print(random_list[-2])

# what if index is out of range
print(random_list[100])  # IndexError: list index out of range
print(random_list[-6])  # IndexError: list index out of range

# tuple - sequence of elements of any data type which DOES NOT grow or shrink (immutable) - normal brackets
# why does not grow - it's directly connected with efficiency
# When to choose - constants or we know that sequence will not change
# Example:
expected_results = ('SUCCESS', 'FAILED', 'PENDING')
print(expected_results)
print(type(expected_results))

# Diff between list and tuple
# lists can grow (mutable) but tuple cannot grow (immutable)
# Access
print(expected_results[-1])
print(expected_results[-2])

# English dictionary - word: meaning
# Mapping of a key (english word) and a value (it's meaning, one or more meaning)
# top - playing item, refers to the top of something
# - multiple - list or a tuple

# dict - collection of key and value pairs within {} curly braces. Each key and value should be sep by ':'
# ordering: py3.7 - ordering is preserved, unordered until - py3.5
# dict keys - unique
car_prices = {'BMW': 1000, 'Ferrari': 2000, 'Toyota': 500}
# car_prices = {'BMW': 1000, 'Ferrari': 2000, 'Toyota': 500, 'BMW': None, 'Ferrari': None, 'Toyota': None}
print(car_prices)
print(type(car_prices))

# Retrieve item
# individual key and value pair is called an item
print("Toyota's price is:", car_prices['Toyota'])

# Access
# list = l[0]
# dict - dictionary[key]
user = {'name': 'swadhi'}
print(user['name'])

# set - sequence of elements of any data type which grow or shrink (mutable) - curly brace sep by comma
#   - Unique elements
#   - Unordered
set_of_students = {'swadhi', 'govind', 'chiranjivi', 'govind', 'chiranjivi', 'swadhi'}
print(set_of_students)

"""
Interview questions covered so far:

    1. Mention the basic datatypes that python supports.
    2. State the difference between single division operator / and integer division operator //
    3. How would you define a multi-line string?
    4. How would you access the last element of a list efficiently?
    5. What is the difference between a list and a tuple?
    6. What are mutable and immutable data types? Give examples.
    7. Does lists, tuples and set allow duplicates? Give examples.
    8. Does dictionary allows duplicate keys?
    9. Does dictionary allows duplicate values?
"""
