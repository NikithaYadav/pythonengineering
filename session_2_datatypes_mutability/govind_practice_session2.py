# #1. Mention the basic datatypes that python supports.
# # Mutable Datatypes : list , Dict,set
# # Immutable Datatypes:bool , int , float , str ,tuple
# # 2. State the difference between single division operator / and integer division operator //
# if we want the data type as integer value for any division which yields float value then we go with //
#
#     3. How would you define a multi-line string?
# We use ''' ''' quotes for multiline string
# mul_line = '''this is first line
# this is second line
#  this is third line'''
# print (mul_line)
#
#     4. How would you access the last element of a list efficiently?
# last_element = ['one','two','three','four','last']
# print(last_element[-1])
#     5. What is the difference between a list and a tuple?
# list[] is mutable  means values in the list are tend to grow  and it does not need any new object to store the data
# tuple() is immutable means values in the tuples does not grow and it needs new object to store the data
#     6. What are mutable and immutable data types? Give examples.
# see 1 answer
#     7. Does lists, tuples and set allow duplicates? Give examples.
# dup_list = [ 'govind','govind','kaluva','sudha']  - Lists allow duplicates
# print(dup_list)
# ['govind', 'govind', 'kaluva', 'sudha']

# dup_tuple = [ 'govind','govind','kaluva','sudha'] - Tuple allow duplicates
# print(dup_tuple)
# ['govind', 'govind', 'kaluva', 'sudha']

# dup_set = {'govind','govind','kaluva','sudha'} - set  does not allow duplicates
# print(dup_set)
# {'govind', 'sudha', 'kaluva'}
#     8. Does dictionary allows duplicate keys?

# dup_dict={'govind':38,'kaluva':40,'sudha':50,'govind':56} - does not allow duplicate keys
# print(dup_dict)

#     9. Does dictionary allows duplicate values?

# dup_dict={'govind':38,'kaluva':40,'sudha':50,'kumar':50} - Allows duplicate values
# print(dup_dict)

